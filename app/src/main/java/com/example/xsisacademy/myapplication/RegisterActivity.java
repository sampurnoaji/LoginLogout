package com.example.xsisacademy.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xsisacademy.myapplication.hari_1.Preferences;

/**
 * Created by XsisAcademy on 21/06/2019.
 */

public class RegisterActivity extends AppCompatActivity{

    private Button btnRegister;
    private EditText etUsername, etPassword, etRePassword;

    private EditText regUser, regPass, rePass;
    private Button btnReg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        toolListener();
    }

    private void toolListener() {
        btnRegister = findViewById(R.id.btnRegister);

        etPassword = findViewById(R.id.etPassword);
        etUsername = findViewById(R.id.etUsername);
        etRePassword = findViewById(R.id.etRePassword);

        btnRegister.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE || i == EditorInfo.IME_NULL){
                    register();
                    return true;
                }
                return false;
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getBaseContext(), "Register", Toast.LENGTH_SHORT).show();
                register();

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Preferences.getLoggedInStatus(getBaseContext())){
            startActivity(new Intent(getBaseContext(), SudahLoginActivity.class));
            finish();
        }
    }

    //mengecek input user dan password dan memberikan akses ke MainActivity
    private void register(){
        //mereset semua error dan fokus menjadi default
        etUsername.setError(null);
        etPassword.setError(null);
        etRePassword.setError(null);
        View fokus = null;
        boolean cancel = false;

        //mengambil text dari username, pass, dan repass dg variabel baru bertipe String
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        String repassword = etRePassword.getText().toString();

        if (TextUtils.isEmpty(username)){
            etUsername.setError("This field is required");
            fokus = etUsername;
            cancel = true;
        }else if (cekUser(username)){
            etUsername.setError("This username already exist");
            fokus = etUsername;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)){
            etPassword.setError("This field is required");
            fokus = etPassword;
            cancel = true;
        }else if (!cekPassword(password, repassword)){
            etPassword.setError("Password does not match");
            fokus = etPassword;
            cancel = true;
        }

        if (cancel){
            fokus.requestFocus();
        }else {
            Preferences.setRegisteredUser(getBaseContext(), username);
            Preferences.setRegisteredPass(getBaseContext(), password);
            finish();
        }
    }

    private boolean cekPassword(String password, String repassword){
        return password.equals(repassword);
    }

    private boolean cekUser(String user){
        return user.equals(Preferences.getRegisteredUser(getBaseContext()));
    }
}
