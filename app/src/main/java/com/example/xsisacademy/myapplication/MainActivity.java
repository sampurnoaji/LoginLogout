package com.example.xsisacademy.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.xsisacademy.myapplication.hari_1.Preferences;

public class MainActivity extends AppCompatActivity {

    private Button btnHari1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolListener();
    }

    private void toolListener(){
        btnHari1 = findViewById(R.id.btnHari1);


        btnHari1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                //finish();
            }
        });
    }
}
