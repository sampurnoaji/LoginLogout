package com.example.xsisacademy.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.xsisacademy.myapplication.hari_1.Preferences;

/**
 * Created by XsisAcademy on 20/06/2019.
 */

public class LoginActivity extends AppCompatActivity{

    private Button btnLogin, btnRegister;
    private EditText etUsername, etPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        toolListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Preferences.getLoggedInStatus(getBaseContext())){
            startActivity(new Intent(getBaseContext(), RegisterActivity.class));
        }
    }

    private void toolListener() {
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);

        etPassword = findViewById(R.id.etPassword);
        etUsername = findViewById(R.id.etUsername);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getBaseContext(), RegisterActivity.class));
            }
        });
    }

    private void login(){
        //mereset semua error dan fokus menjadi default
        etUsername.setError(null);
        etPassword.setError(null);
        View fokus = null;
        boolean cancel = false;

        //mengambil text dari username, pass, dan repass dg variabel baru bertipe String
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        if (TextUtils.isEmpty(username)){
            etUsername.setError("This field is required");
            fokus = etUsername;
            cancel = true;
        }else if (!cekUser(username)){
            etUsername.setError("Username is not listed");
            fokus = etUsername;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)){
            etPassword.setError("This field is required");
            fokus = etPassword;
            cancel = true;
        }else if (!cekPassword(password)){
            etPassword.setError("Password does not match");
            fokus = etPassword;
            cancel = true;
        }

        if (cancel){
            fokus.requestFocus();
        }else {
            suksesLogin();
        }
    }

    private void suksesLogin(){
        Preferences.setLoggedInUser(getBaseContext(), Preferences.getRegisteredUser(getBaseContext()));
        Preferences.setLoggedInStatus(getBaseContext(), true);
        Toast.makeText(this, "Sukses login activity", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(LoginActivity.this, SudahLoginActivity.class));
        finish();
    }

    private boolean cekPassword(String password){
        return password.equals(Preferences.getRegisteredPass(getBaseContext()));
    }

    private boolean cekUser(String user){
        return user.equals(Preferences.getRegisteredUser(getBaseContext()));
    }
}
