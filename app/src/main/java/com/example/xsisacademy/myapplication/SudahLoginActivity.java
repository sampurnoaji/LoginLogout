package com.example.xsisacademy.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.xsisacademy.myapplication.hari_1.Preferences;

/**
 * Created by XsisAcademy on 21/06/2019.
 */

public class SudahLoginActivity extends AppCompatActivity{
    Button btnLogout;
    TextView txt;
    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.activity_sudah_login);

        txt = (TextView) findViewById(R.id.textYuhu);
        txt.setText("Welcome  "+Preferences.getLoggedInUser(getApplicationContext()) + "  password kamu : " + Preferences.getRegisteredPass(getApplicationContext()) );
        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.clearLoggedInUser(getApplicationContext());
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
