package com.example.xsisacademy.myapplication.hari_1;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by XsisAcademy on 20/06/2019.
 */

public class Preferences {
    static final String KEY_USER_TEREGISTER = "user", KEY_PASS_TEREGISTER = "pass";
    static final String KEY_USERNAME_SEDANG_LOGIN = "Username_logged_in";
    static final String KEY_STATUS_SEDANG_LOGIN = "Status_logged_in";

    //pendlekarasian shared preferences yang berdasarkan paramater context
    private static SharedPreferences getSharedPreference(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    //deklarasi edit preferense dan mengubah data
    //yang memiliki key keyy_pass_tereggister dengan parameter password
    public static void setRegisteredUser(Context context, String username){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USER_TEREGISTER, username);
        editor.apply();
    }

    //mengembalikan nilai dari key key_user_teregister berupa string
    public static String getRegisteredUser(Context context){
        return getSharedPreference(context).getString(KEY_USER_TEREGISTER, "");
    }

    //deklarasi edit preferences dan mengubah data
    //yang memiliki key key_pass_teregister dengan parameter password
    public static void setRegisteredPass(Context context, String password){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_PASS_TEREGISTER, password);
        editor.apply();
    }
    //mengembalika nilai dari key key_pass_teregister berupa string
    public static String getRegisteredPass(Context context){
        return getSharedPreference(context).getString(KEY_PASS_TEREGISTER, "");

    }
    //deklarasi edit preferences dan mengubah data
    //yang memiliki key key_username_Sedang_login dengan parameter username
    public static void setLoggedInUser(Context context, String username){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USERNAME_SEDANG_LOGIN, username);
        editor.apply();
    }
    //mengembalikan nilai dari key key_username_sedang_login dengan parameter username
    public static String getLoggedInUser(Context context){
        return getSharedPreference(context).getString(KEY_USERNAME_SEDANG_LOGIN,"");
    }
    //deklarasi edit preferences dan mengubah data
    //yang memiliki key key_status sedang login dengan parameter status
    public static void setLoggedInStatus(Context context, boolean status){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putBoolean(KEY_STATUS_SEDANG_LOGIN, status);
        editor.apply();
    }
    public static boolean getLoggedInStatus(Context context){
        return getSharedPreference(context).getBoolean(KEY_STATUS_SEDANG_LOGIN, false);
    }

    //deklarasi edit preferences dan menghapus data, sehingga menjadikan nilai default
    //khusus data yang memiliki key key_username_sedang_login dan key_status_sedang_login
    public static void clearLoggedInUser (Context context){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(KEY_USERNAME_SEDANG_LOGIN);
        editor.remove(KEY_STATUS_SEDANG_LOGIN);
        editor.apply();
    }
}
